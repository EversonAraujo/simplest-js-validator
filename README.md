# Simplest Js Validator

### Installation

Manually
```html 
	<script src="../validator.js">
```
Using NPM

```terminal 
	npm install --save simplest-js-validator
```

### How to use

*For use name or data attributes see configuration below*

**HTML**
```html
<input id="username"></input>
<div id="usernameErrors" hidden></div>

<!-- Using VueJs -->
<div v-if="hasError('username')">
	<small v-for="e in errors.username">{{ e }}</small>
</div>
```

** JS **
```js
let userValidator = {
	username: 'required, email',
	birthday: 'required, regex:^\\d{2}\\/\\d{2}\\/\\d{4}$',
}

let validator = new SimplestValidator();
validator.validate(userValidator).then((validation) => {
	if (validation.isValid) {
		// Do something...
	} else {
		validation.errors.forEach((e) => {
			let small = document.createElement('small').text = e;
			document.querySelect('#'+ e + 'Error').append(small);
			document.querySelect('#'+ e + 'Error').removeAttr('hidden');
		})
		
		// ON VueJS
		
		this.errors = validation.errors
		
		data {
			errors: []
		}
		...
		methods: {
			hasError(field) {
				return Object.keys(this.errors).indexOf(field) > -1
			},
			onSubmit ()
		}
	}
}).catch((e) => {
	// Ops! Error, verify identifiers
	console.log(e)
})
```
---
### Configurations
```js
	let validator = new SimplestValidator({
		findBy: 'name'
		invalid: '%s is invalid',
        missing: '%s is required'
	})
```
### Available Validators

| Rule | Description |
| --- | --- |
| **alpha** | For letters and spaces `/^[ a-z]+$/i` |
| **email** | For email |
| **regex** | For custom regex |
| **required** | Avoid empty field |


*Here some regex for we place by default on this validator, [thanks for nerdsrescueme](https://gist.github.com/nerdsrescueme/1237767)*

## Considerations

Thank you, and please do not judge! Give suggestions and help, I will be grateful for your contribution.
As soon as I have more time I'll improve this little validator and create as a VueJs component or plugin.