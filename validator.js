/**
 * Author Everson Araújo
 * Created 15/02/18
 */
export default function SimplestValidator() {

    const ALPHA = 'alpha';
    const EMAIL = 'email';
    const DIGITS = 'digits';
    const REQUIRED = 'required';

    let defaults = {
        invalid: 'O campo %s é inválido',
        missing: 'O campo %s é obrigatório'
    };

    if (arguments) {
        if(typeof arguments[0] === "object")
            this.options = extendDefaults(defaults, arguments[0]);
    }

    function extendDefaults(source, properties) {
        let property;
        for (property in properties) {
            if (properties.hasOwnProperty(property)) {
                source[property] = properties[property];
            }
        }
        return source;
    }

    function doValidation (validable) {

        let global = {};

        for (let prop in validable) {

            if (validable.hasOwnProperty(prop) && typeof validable[prop] === 'object') {
                let result = doValidation(validable[prop]);

                if (Object.keys(result).length > 0) {
                    Object.assign(global, result)
                }
            } else if (validable.hasOwnProperty( prop )) {

                let errors = [];

                try {

                    let rules = validable[prop].split(',');
                    let value = document.querySelector('[id$='+ prop +']').value;

                    for (let i = 0; i < rules.length ;i++) {

                        let rule = rules[i].trim();
                        const regEmail = /^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/i;

                        try {
                            if ( REQUIRED === rule && !value) {
                                errors.push(defaults.missing.replace('%s', prop));
                                break
                            } else if (ALPHA === rule && !/^[ A-zÀ-ú]+$/i.test(value)) {
                                errors.push(defaults.invalid.replace('%s', prop))
                            } else if (DIGITS === rule && !/^[ \d]+$/i.test(value)) {
                                errors.push(defaults.invalid.replace('%s', prop))
                            } else if (EMAIL === rule && !regEmail.test(value)) {
                                errors.push(defaults.invalid.replace('%s', prop))
                            } else if (/^regex:/i.test(rule) &&
                                !new RegExp(rule.replace('regex:', '').trim()).test(value)) {
                                errors.push(defaults.invalid.replace('%s', prop))
                            }
                        } catch (err) {
                            Error('Regex invalid for: '+ prop )
                        }
                    }

                    if (errors.length > 0) {
                        global[prop] = errors
                    }
                } catch (error) {
                    global[prop] = ['Not found field for id: ' + prop ]
                }
            }
        }

        return global
    }

    SimplestValidator.prototype.validate = function (validable) {
        return new Promise((resolve, reject) => {
            let global = doValidation(validable);

            if (Object.keys(global).length === 0) {
                resolve({ isValid: true, errors: global })
            } else {
                reject({ isValid: false, errors: global })
            }

        })
    }
}
